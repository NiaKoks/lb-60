import React, {Component} from 'react';
import './Messages.css';

class Messages extends Component {
    render() {
        return (
            <div className="Messages">
                <span className="author">{this.props.author}:</span>
                <span>{this.props.messages}</span>
            </div>
        );
    }
}

export default Messages;