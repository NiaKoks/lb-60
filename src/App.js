import React, { Component } from 'react';
import './App.css';
import ChatBody from "./components/ChatBody/ChatBody";
import Messages from "./components/Messages/Messages";

class App extends Component {
  state={
    messages:[],
      author: '',
      value: '',
  };

  shouldComponentUpdate(nextProps, nextState){
    return this.state.messages.length !== nextState.messages.length
  }

    mesageLoad =(lastdata)=>{
        console.log(lastdata);
        const getmessagesURL = ' http://146.185.154.90:8000/messages';
    const getLastMessagesURL = ' http://146.185.154.90:8000/messages?datetime=' + lastdata;

        clearInterval(this.interval);

    this.interval = setInterval(()=>{
              fetch(lastdata? getLastMessagesURL: getmessagesURL).then(response =>{
            if(response.ok){
                return response.json()
            }
            throw new Error ('Something wrong with request')
        }).then(messages =>{
            const Posts= messages.map(mess => ({
                ...mess
            }));
            this.setState({messages: [...this.state.messages].concat(Posts)});
        }).catch(error =>{
            console.log(error);
        })},3000)
    }

    componentDidMount(){
        this.mesageLoad();
    }

    componentDidUpdate(){
      console.log("last date", this.state.messages[this.state.messages.length -1].datetime);
        this.mesageLoad(this.state.messages[this.state.messages.length -1].datetime);
    }

    changeHandler = event => {
        this.setState({author: event.target.value})
    };

    textChange = (event) => {
        this.setState({
            value: event.target.value
        })
    };

    postMessage = ()=>{
    const url= 'http://146.185.154.90:8000/messages';
    const data = new URLSearchParams();
        data.set('message', this.state.value);
        data.set('author', this.state.author);


        fetch(url,{
          method:'post',
            body:data,
        }).then(() => {
            this.setState({
                value: '',
                author: ''
            })
        });
    }


    render() {
    return (
      <div className="App">
        <ChatBody send={this.postMessage} changeHandler={this.changeHandler} textChange={this.textChange}/>
          {this.state.messages.map((message,id)=><Messages messages={message.message} author={message.author} key={id}/>)}

      </div>
    );
  }
}

export default App;
